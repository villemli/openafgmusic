from django.db import models


class Artist(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Video(models.Model):
    title = models.CharField(max_length=255)
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE, related_name='videos')
    description = models.TextField(blank=True)
    youtubeId = models.CharField(max_length=11)
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.artist}: {self.title}'
