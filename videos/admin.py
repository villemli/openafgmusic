from django.contrib import admin

from .models import Artist, Video

admin.site.register(Artist)
admin.site.register(Video)
