from django.views import generic

from videos.models import Video


class IndexView(generic.ListView):
    # TODO: only latest videos
    template_name = 'videos/index.html'
    queryset = Video.objects.order_by('-added_on')


class VideoDetailView(generic.DetailView):
    model = Video

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['related_videos'] = Video.objects.exclude(id=self.object.id).order_by('-added_on')
        return context
